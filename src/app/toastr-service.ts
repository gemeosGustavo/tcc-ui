import { ToastrService } from "ngx-toastr";



export class ToastService {

    constructor(private toastr: ToastrService) {

    }

    showSuccess(title: string, message: string) {
        this.toastr.success(title, message);
    }

    showError(title: string, message: string) {
        this.toastr.error(title, message);
    }

    showWarning(title: string, message: string) {
        this.toastr.warning(title, message);
    }

    showInfo(title: string, message: string) {
        this.toastr.info(title, message);
    }
}