import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LbdModule } from "app/lbd/lbd.module";
import { LoginComponent } from "./login.component";
import { NgModule } from "@angular/core";
import { loginRoutingModule } from "./login.routing.module";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TokenStorageService } from "./service/token-storage.service";

@NgModule({
    imports: [
      CommonModule,
      loginRoutingModule,
      ReactiveFormsModule,
      HttpClientModule,
      HttpClient,
      LbdModule
    ],
    declarations: [
      TokenStorageService,
    ],
    providers: [
      LoginComponent
    ]
  })
  
  export class LoginModule {}