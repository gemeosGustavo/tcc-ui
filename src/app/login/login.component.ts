import { Component, OnInit, EventEmitter } from '@angular/core';
import { TokenStorageService } from './service/token-storage.service';
import { AuthService } from './service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserLogin } from './model/login';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private usuarioAutenticado: boolean = false;
  mostrarMenuEmitter = new EventEmitter<boolean>();

  formLogin: FormGroup;
  userLogin: UserLogin = new UserLogin();

  constructor(
    formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private token: TokenStorageService,
    private toasterService: ToastrService

  ) {
    this.formLogin = formBuilder.group({
      username: [''],
      password: ['']
    });
  }

  ngOnInit() {
  }

  login(): void {
    this.authService.fazerLogin(this.userLogin.username, this.userLogin.password).subscribe(
      data => {

        this.token.saveToken(data.token);
        this.token.saveUsername(this.userLogin.username);
        this.router.navigate(['/relatorio']);
      },
      err => {
        this.toasterService.error("Usuario ou senha estão incorretos.");
      }
    );
  }

  usuarioEstaAutenticado() {
    return this.usuarioAutenticado;
  }

  logout() {

  }
}
