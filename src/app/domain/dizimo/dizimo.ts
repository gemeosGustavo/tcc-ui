import { Pessoa } from "../pessoa/pessoa";

export class Dizimo {
    id: number;
    valor_dizimo: number;
    data_dizimo: Date;
    pessoaid: Pessoa = new Pessoa()
}