
export class Endereco {
    id: number;
    logradouro: string;
    bairro: string;
    cidade: string;
    uf: string;
    excluido: number
}