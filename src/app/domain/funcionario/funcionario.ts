import { Pessoa } from "../pessoa/pessoa";

export class Funcionario {
    id: number;
    cargo: string;
    salario: number;
    pessoaid: Pessoa = new Pessoa();
    excluido: number
}
