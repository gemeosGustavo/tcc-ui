import { Endereco } from "../endereco/endereco";

export class Pessoa {
    id: number;
    nome: string;
    telefone: string;
    data_nascimento: Date;
    enderecoid: Endereco = new Endereco();
    numero_casa: number;
    excluido: number
}