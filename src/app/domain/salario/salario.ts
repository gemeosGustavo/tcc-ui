import { Funcionario } from "../funcionario/funcionario";

export class Salario {
    id: number;
    valor_pagamento: number;
    data_pagamento_salario: Date;
    descricao_pagamento: string;
    funcionarioid: Funcionario = new Funcionario()
}