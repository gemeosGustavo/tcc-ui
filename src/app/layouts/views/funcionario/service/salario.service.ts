import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Salario } from 'app/domain/salario/salario';

@Injectable()
export class SalarioService {

  private url: string = 'http://localhost:7765/salario';

  constructor(private http: HttpClient) { }

  getSalarios(): Observable<Salario[]>{
    return this.http.get<Salario[]>(this.url + "/dados");
  }

  getSalario(id): Observable<Salario>{
    return this.http.get<Salario>(this.url + "/dados/" + id);
  }

  addSalario(salario){
    return this.http.post(this.url + "/adicionar", JSON.stringify(salario));
  }

  updateSalario(salario){
    return this.http.put(this.url + "/atualizar/" + salario.id, JSON.stringify(salario));
  }

  deleteSalario(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}
