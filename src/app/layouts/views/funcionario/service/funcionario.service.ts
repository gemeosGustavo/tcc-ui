import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Funcionario } from 'app/domain/funcionario/funcionario';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FuncionarioService {

  private url: string = 'http://localhost:7765/funcionario';

  constructor(private http: HttpClient) { }

  getFuncionarios(): Observable<Funcionario[]>{
    return this.http.get<Funcionario[]>(this.url + "/dados");
  }

  getFuncionario(id): Observable<Funcionario>{
    return this.http.get<Funcionario>(this.url + "/dados/" + id);
  }

  addFuncionario(funcionario){
    return this.http.post(this.url + "/adicionar", JSON.stringify(funcionario));

    }

  updateFuncionario(funcionario){
    return this.http.put(this.url + "/atualizar/" + funcionario.id, JSON.stringify(funcionario));
  }

  deleteFuncionarioa(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}