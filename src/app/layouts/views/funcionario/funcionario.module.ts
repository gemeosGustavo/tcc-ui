import { NgModule } from "@angular/core";
import { MaterialModule } from "app/angular-material/material-module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { FuncionarioRoutingModule } from "./funcionario.routing.module";
import { FuncionarioService } from "./service/funcionario.service";
import { FuncionarioComponent } from "./funcionario.component";
import { FuncionarioFilterPipe } from "./filter/funcionario.filter";
import { PessoaService } from "../pessoa/service/pessoa.service";
import { SalarioService } from "./service/salario.service";
import { LbdModule } from "app/lbd/lbd.module";
import { SidebarModule } from "app/sidebar/sidebar.module";
import { NavbarModule } from "app/shared/navbar/navbar.module";
import { FooterModule } from "app/shared/footer/footer.module";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "app/auth-interceptor";

@NgModule({
    imports: [
        HttpClientModule,
        MaterialModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LbdModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FuncionarioRoutingModule

    ],
    declarations: [
        FuncionarioComponent,
        FuncionarioFilterPipe,
    ],
    providers: [
        FuncionarioService,
        PessoaService,
        SalarioService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ]
})
export class FuncionarioModule {}