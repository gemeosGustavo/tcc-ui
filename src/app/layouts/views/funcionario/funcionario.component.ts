import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Funcionario } from 'app/domain/funcionario/funcionario';
import { Pessoa } from 'app/domain/pessoa/pessoa';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { FuncionarioService } from './service/funcionario.service';
import { PessoaService } from 'app/layouts/views/pessoa/service/pessoa.service';
import { SalarioService } from './service/salario.service';
import { Salario } from 'app/domain/salario/salario';

@Component({
  selector: 'app-funcionario',
  templateUrl: './funcionario.component.html',
  styleUrls: ['./funcionario.component.scss']
})
export class FuncionarioComponent implements OnInit {

  formFuncionario: FormGroup;
  formSalario: FormGroup;
  funcionario: Funcionario = new Funcionario();
  funcionarios: Funcionario[];
  salario: Salario = new Salario();
  pessoas: Pessoa[];

  displayedColumns = ['cod', 'cargo', 'salario', 'pessoaid', 'editar', 'excluir'];
  dataSource: MatTableDataSource<Funcionario>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    formBuilder: FormBuilder,
    private toast: ToastrService,
    private funcionarioService: FuncionarioService,
    private pessoaService: PessoaService,
    private salarioService: SalarioService

  ) {
    this.formFuncionario = formBuilder.group({
      id: [''],
      cargo: ['', Validators.required],
      salario: ['', Validators.required],
      pessoaid: formBuilder.group({
        id: ['', Validators.required]
      })
    });
    this.formSalario = formBuilder.group({
      id: [''],
      valor_pagamento: ['', Validators.required],
      data_pagamento_salario: ['', Validators.required],
      descricao_pagamento: ['', Validators.required],
      funcionarioid: formBuilder.group({
        id: ['', Validators.required]
      })
    });
    this.dataSource = new MatTableDataSource(this.funcionarios);
  }

  ngOnInit() {

    this.initializerObjects();

    $("#funcionario").hide();
    $("#listaFuncionarios").hide();
    $("#salario").hide();
  }

  mostrarFuncionarios() {
    $("#listaFuncionarios").toggle("slow");
  }

  mostrarSalario() {
    $("#salario").toggle("slow");
  }

  mostrarCadastroFuncionario() {
    $("#funcionario").toggle("slow");
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  popularDadosFormFunc(dados) {
    this.formFuncionario.patchValue({
      id: dados.id,
      pessoaid: dados.pessoaid,
      cargo: dados.cargo,
      salario: dados.salario
    });
  }

  initializerObjects(){
    this.pessoaService.getPessoas()
      .subscribe(pessoas => this.pessoas = pessoas);

    this.funcionarioService.getFuncionarios()
      .subscribe(funcionarios => this.funcionarios = funcionarios);

    this.funcionarioService.getFuncionarios()
      .subscribe(res => {
        this.dataSource.data = res as Funcionario[];
      });
  }

  sendSaveRequest() {
    var result,
      funcionarioValue = this.formFuncionario.value;

    if (funcionarioValue.id) {
      console.log("UPDATE")
      result = this.funcionarioService.updateFuncionario(funcionarioValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Funcionário editado com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
          }
        );
    } else {
      console.log("ADICIONAR")
      result = this.funcionarioService.addFuncionario(funcionarioValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Funcionário cadastrado com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar cadastrar um novo funcionário');
          }
        );
    }
  }

  excluir(funcionario) {
    let resp = confirm('Deseja mesmo excluir este funcionário ?');
    if (resp === true) {
      this.funcionarioService.deleteFuncionarioa(funcionario.id)
        .subscribe(
          suc => {
            this.toast.warning('', 'Registro excluido com sucesso. ');
            this.initializerObjects();
          },
          err => {
            this.toast.error('Verifique com o administrador se o seu usuario possui permissão', 'Ocorreu um erro ao tentar excluir o registro');
          }
        )
    } else {
      return;
    }
  }

  editar(funcionario) {
    $("#funcionario").toggle("slow");
    this.popularDadosFormFunc(funcionario);
  }

  salvarSalario() {
    var result,
      salarioValue = this.formSalario.value

      result = this.salarioService.addSalario(salarioValue)
      .subscribe(
          suc => {
              this.toast.success('', 'Salário lançado com sucesso');
              location.reload();
          },
          err => {
              this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar lançar um novo salário');
          }
      );

  }

}