import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'funcionarioFilter'})
export class FuncionarioFilterPipe implements PipeTransform{
    transform(value: any[], term: string): any[]{
        return value.filter((x:any) => x.cargo.toLowerCase().startsWith(term.toLowerCase()))
    }
}