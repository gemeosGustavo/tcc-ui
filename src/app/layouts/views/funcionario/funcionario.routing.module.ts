import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { FuncionarioComponent } from "./funcionario.component";
import { AuthGuard } from "app/guard/AuthGuard";


const funcionarioRoutes: Routes = [
    { path: '', component: FuncionarioComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(funcionarioRoutes)],
    exports: [RouterModule]
})

export class FuncionarioRoutingModule {
    
}