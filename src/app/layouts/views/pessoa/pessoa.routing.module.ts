import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { PessoasComponent } from "./pessoas.component";
import { AuthGuard } from "app/guard/AuthGuard";


const pessoaRoutes: Routes = [
    { path: '', component: PessoasComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(pessoaRoutes)],
    exports: [RouterModule]
})

export class PessoaRoutingModule {
    
}