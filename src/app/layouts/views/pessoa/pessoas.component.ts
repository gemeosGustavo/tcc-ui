import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Endereco } from 'app/domain/endereco/endereco';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Pessoa } from 'app/domain/pessoa/pessoa';
import { EnderecoService } from 'app/layouts/views/endereco/service/endereco.service';
import { PessoaService } from './service/pessoa.service';

@Component({
  selector: 'app-pessoas',
  templateUrl: './pessoas.component.html',
  styleUrls: ['./pessoas.component.scss']
})
export class PessoasComponent implements OnInit {

  formPessoa: FormGroup;
  pessoa: Pessoa = new Pessoa();
  pessoas: Pessoa[];
  enderecos: Endereco[];
  query: string = ''

  displayedColumns = ['cod', 'nome', 'telefone', 'dataNascimento', 'endereco', 'numeroCasa', 'editar', 'excluir'];
  dataSource: MatTableDataSource<Pessoa>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    formBuilder: FormBuilder,
    private toastr: ToastrService,
    private enderecoService: EnderecoService,
    private pessoaService: PessoaService
  ) {
    this.formPessoa = formBuilder.group({
      id: [''],
      nome: ['', Validators.required],
      telefone: ['', Validators.required],
      data_nascimento: ['', Validators.required],
      enderecoid: formBuilder.group({
        id: ['', Validators.required]
      }),
      numero_casa:['', Validators.required]
    });
    
    this.dataSource = new MatTableDataSource(this.pessoas);
  }

  ngOnInit() {

    this.initializeObjects();

    $("#cadastro").hide();
    $("#listaPessoas").hide();
  }

  mostrarPessoas() {
    $("#listaPessoas").toggle("slow");
  }

  mostrarCadastro() {
    $("#cadastro").toggle("slow");
  }

  popularDadosForm(dados) {
    this.formPessoa.patchValue({
      id: dados.id,
      nome: dados.nome,
      telefone: dados.telefone,
      data_nascimento: dados.data_nascimento,
      enderecoid: dados.enderecoid,
      numero_casa: dados.numero_casa
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  sendSaveRequest() {
    var result,
      pessoaValue = this.formPessoa.value;

    if (pessoaValue.id) {
      console.log("UPDATE")
      result = this.pessoaService.updatePessoa(pessoaValue)
        .subscribe(
          suc => {
            this.toastr.success('', 'Pessoa editada com sucesso');
            location.reload();
          },
          err => {
            this.toastr.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
          }
        );
    } else {
      console.log("ADICIONAR")
      result = this.pessoaService.addPessoa(pessoaValue)
        .subscribe(
          suc => {
            this.toastr.success('', 'Pessoa cadastrada com sucesso');
            location.reload();
          },
          err => {
            this.toastr.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar cadastrar uma nova pessoa');
          }
        );
    }
  }

  editar(pessoa) {
    $("#cadastro").toggle("slow");
    this.popularDadosForm(pessoa);
  }

  excluir(pessoa) {
    let resp = confirm('Deseja mesmo excluir esta pessoa ?');
    if (resp === true) {
      this.pessoaService.deletePessoa(pessoa.id)
        .subscribe(
          suc => {
            this.toastr.warning('', 'Registro excluido com sucesso. ');
            this.initializeObjects();
          },
          err => {
            this.toastr.error('Verifique com o administrador se o seu usuario possui permissão', 'Ocorreu um erro ao tentar excluir o registro');
          }
        )
    } else {
      return;
    }
  }

  initializeObjects() {
    this.enderecoService.getEnderecos()
      .subscribe(enderecos => this.enderecos = enderecos);

    this.pessoaService.getPessoas()
      .subscribe(res => {
        this.dataSource.data = res as Pessoa[];
      });
  }

}

