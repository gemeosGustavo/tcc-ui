import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'pessoaFilter'})
export class PessoaFilterPipe implements PipeTransform{
    transform(value: any[], term: string): any[]{
        return value.filter((x:any) => x.nome.toLowerCase().startsWith(term.toLowerCase()))
    }
}