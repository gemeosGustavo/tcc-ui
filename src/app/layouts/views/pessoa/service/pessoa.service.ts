import { Injectable } from '@angular/core';;
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pessoa } from 'app/domain/pessoa/pessoa';

@Injectable()
export class PessoaService {

  private url: string = 'http://localhost:7765/pessoa';

  constructor(private http: HttpClient) { }

  getPessoas(): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>(this.url + "/dados");
  }

  getPessoa(id): Observable<Pessoa> {
    return this.http.get<Pessoa>(this.url + "/dados/" + id);
  }

  addPessoa(pessoa) {
    return this.http.post(this.url + "/adicionar", JSON.stringify(pessoa));
  }

  updatePessoa(pessoa) {
    return this.http.put(this.url + "/atualizar/" + pessoa.id, JSON.stringify(pessoa));
  }

  deletePessoa(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}
