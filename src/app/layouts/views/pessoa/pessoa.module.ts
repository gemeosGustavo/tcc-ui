import { NgModule } from "@angular/core";
import { MaterialModule } from "app/angular-material/material-module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { PessoaRoutingModule } from "./pessoa.routing.module";
import { PessoasComponent } from "./pessoas.component";
import { PessoaFilterPipe } from "./filter/pessoa.filter";
import { PessoaService } from "./service/pessoa.service";
import { LbdModule } from "app/lbd/lbd.module";
import { SidebarModule } from "app/sidebar/sidebar.module";
import { NavbarModule } from "app/shared/navbar/navbar.module";
import { FooterModule } from "app/shared/footer/footer.module";
import { EnderecoService } from "../endereco/service/endereco.service";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "app/auth-interceptor";
import { EnderecoComponent } from "../endereco/endereco.component";

@NgModule({
    imports: [
        MaterialModule,
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule,
        LbdModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        PessoaRoutingModule

    ],
    declarations: [
        PessoasComponent,
        PessoaFilterPipe
    ],
    providers: [
        PessoaService,
        EnderecoService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ]
})
export class PessoaModule {}