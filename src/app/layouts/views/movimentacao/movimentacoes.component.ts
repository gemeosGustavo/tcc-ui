import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Dizimo } from 'app/domain/dizimo/dizimo';
import { Oferta } from 'app/domain/oferta/oferta';
import { Missao } from 'app/domain/missao/missao';
import { Conta } from 'app/domain/conta/conta';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Pessoa } from 'app/domain/pessoa/pessoa';
import { ToastrService } from 'ngx-toastr';
import { PessoaService } from '../pessoa/service/pessoa.service';
import { DizimoService } from './service/dizimo.service';
import { OfertaService } from './service/oferta.service';
import { MissaoService } from './service/missao.service';
import { ContaService } from './service/conta.service';
@Component({
  selector: 'app-movimentacoes',
  templateUrl: './movimentacoes.component.html',
  styleUrls: ['./movimentacoes.component.scss']
})
export class MovimentacoesComponent implements OnInit {

  formDizimo: FormGroup;
  pessoa: FormGroup;
  dizimo: Dizimo = new Dizimo();
  dizimos: Dizimo[];
  pessoas: Pessoa[];

  formOferta: FormGroup;
  oferta: Oferta = new Oferta();
  ofertas: Oferta[];

  formMissao: FormGroup;
  missao: Missao = new Missao();
  missoes: Missao[];

  formConta: FormGroup;
  conta: Conta = new Conta();
  contas: Conta[];

  query: string = ''

  displayedColumnsDizimo = ['cod', 'pessoaid', 'valorDizimo', 'dataDizimo', 'editar', 'excluir'];
  dataSourceDizimo: MatTableDataSource<Dizimo>;

  displayedColumnsOferta = ['cod', 'valorOferta', 'dataOferta', 'editar', 'excluir'];
  dataSourceOferta: MatTableDataSource<Oferta>;

  displayedColumnsMissao = ['cod', 'valorMissao', 'dataOfertaMissao', 'editar', 'excluir'];
  dataSourceMissao: MatTableDataSource<Missao>;

  displayedColumnsConta = ['cod', 'valorConta', 'dataPagamento', 'descricao', 'editar', 'excluir'];
  dataSourceConta: MatTableDataSource<Conta>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(
    formBuilder: FormBuilder,
    private toast: ToastrService,

    private dizimoService: DizimoService,
    private pessoaService: PessoaService,
    private ofertaService: OfertaService,
    private missaoService: MissaoService,
    private contaService: ContaService

  ) {
    this.formDizimo = formBuilder.group({
      id: [''],
      pessoaid: formBuilder.group({
        id: ['', Validators.required]
      }),
      valor_dizimo: ['', Validators.required],
      data_dizimo: ['', Validators.required],
    });

    this.formOferta = formBuilder.group({
      id: [''],
      valor_oferta: ['', Validators.required],
      data_oferta: ['', Validators.required],
    });

    this.formMissao = formBuilder.group({
      id: [''],
      valor_missao: ['', Validators.required],
      data_oferta_missao: ['', Validators.required],
    });

    this.formConta = formBuilder.group({
      id: [''],
      valor_conta: ['', Validators.required],
      data_pagamento: ['', Validators.required],
      descricao: ['', Validators.required]
    });

    this.dataSourceDizimo = new MatTableDataSource(this.dizimos);
    this.dataSourceOferta = new MatTableDataSource(this.ofertas);
    this.dataSourceMissao = new MatTableDataSource(this.missoes);
    this.dataSourceConta = new MatTableDataSource(this.contas);

  }

  ngOnInit() {

    this.initializeObjects();
    
    $("#oferta").hide();
    $("#listaOfertas").hide();

    $("#missoes").hide();
    $("#listaMissoes").hide();

    $("#dizimo").hide();
    $("#listaDizimos").hide();

    $("#conta").hide();
    $("#listaContas").hide();

  }

  mostrarOfertas() {
    $("#mostrarMissoes").toggle("fast");
    $("#mostrarLancamentoMissoes").toggle("fast");
    $("#mostrarDizimo").toggle("fast");
    $("#mostrarLancamentoDizimo").toggle("fast");
    $("#mostrarContas").toggle("fast");
    $("#mostrarLancamentoContas").toggle("fast");
    $("#listaOfertas").toggle("slow");

  }
  mostrarLancamentoOfertas() {
    $("#mostrarMissoes").toggle("fast");
    $("#mostrarLancamentoMissoes").toggle("fast");
    $("#mostrarDizimo").toggle("fast");
    $("#mostrarLancamentoDizimo").toggle("fast");
    $("#mostrarContas").toggle("fast");
    $("#mostrarLancamentoContas").toggle("fast");
    $("#oferta").toggle("slow");
  }

  mostrarMissoes() {
    $("#mostrarDizimo").toggle("fast");
    $("#mostrarLancamentoDizimo").toggle("fast");
    $("#mostrarContas").toggle("fast");
    $("#mostrarLancamentoContas").toggle("fast");
    $("#listaMissoes").toggle("slow");
  }
  mostrarLancamentoMissoes() {
    $("#mostrarDizimo").toggle("fast");
    $("#mostrarLancamentoDizimo").toggle("fast");
    $("#mostrarContas").toggle("fast");
    $("#mostrarLancamentoContas").toggle("fast");
    $("#missoes").toggle("slow");
  }

  mostrarDizimo() {
    $("#mostrarContas").toggle("fast");
    $("#mostrarLancamentoContas").toggle("fast");
    $("#listaDizimos").toggle("slow");
  }
  mostrarLancamentoDizimo() {
    $("#mostrarContas").toggle("fast");
    $("#mostrarLancamentoContas").toggle("fast");
    $("#dizimo").toggle("slow");
  }

  mostrarContas() {
    $("#listaContas").toggle("slow");
  }
  mostrarLancamentoContas() {
    $("#conta").toggle("slow");
  }

  
  ngAfterViewInit() {
    this.dataSourceDizimo.paginator = this.paginator;
    this.dataSourceDizimo.sort = this.sort;

    this.dataSourceOferta.paginator = this.paginator;
    this.dataSourceOferta.sort = this.sort;

    this.dataSourceMissao.paginator = this.paginator;
    this.dataSourceMissao.sort = this.sort;

    this.dataSourceConta.paginator = this.paginator;
    this.dataSourceConta.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSourceDizimo.filter = filterValue;
    this.dataSourceOferta.filter = filterValue;
    this.dataSourceMissao.filter = filterValue;
    this.dataSourceConta.filter = filterValue;

  }

  popularDadosFormDizimo(dados) {
    this.formDizimo.patchValue({
      id: dados.id,
      valor_dizimo: dados.valor_dizimo,
      data_dizimo: dados.data_dizimo
    });
  }

  popularDadosFormOferta(dados) {
    this.formOferta.patchValue({
      id: dados.id,
      valor_oferta: dados.valor_oferta,
      data_oferta: dados.data_oferta
    });
  }

  popularDadosFormMissao(dados) {
    this.formMissao.patchValue({
      id: dados.id,
      valor_missao: dados.valor_missao,
      data_oferta_missao: dados.data_oferta_missao
    });
  }

  popularDadosFormConta(dados) {
    this.formConta.patchValue({
      id: dados.id,
      valor_conta: dados.valor_conta,
      data_pagamento: dados.data_pagamento,
      descricao: dados.descricao
    });
  }

  editarDizimo(dizimo) {
    
    $("#dizimo").toggle("slow");
    this.popularDadosFormDizimo(dizimo);
  }

  editarOferta(oferta) {

    $("#oferta").toggle("slow");
    this.popularDadosFormOferta(oferta);
  }

  editarMissao(missao){

    $("#missoes").toggle("slow");
    this.popularDadosFormMissao(missao);
  }

  editarConta(conta) {

    $("#conta").toggle("slow");
    this.popularDadosFormConta(conta);
  }

  sendSaveRequestDizimo() {
    var result,
      dizimoValue = this.formDizimo.value;

    if (dizimoValue.id) {
      console.log("UPDATE")
      result = this.dizimoService.updateDizimo(dizimoValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Dizimo editado com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
          }
        );
    } else {
      console.log("ADICIONAR")
      result = this.dizimoService.addDizimo(dizimoValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Dizimo cadastrado com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar lançar um novo dízimo');
          }
        );
    }
  }

  excluirDizimo(dizimo) {
    let resp = confirm('Deseja mesmo excluir este dizimo ?');
    if (resp === true) {
      this.dizimoService.deleteDizimo(dizimo.id)
        .subscribe(
          suc => {
            this.toast.warning('', 'Registro excluido com sucesso. ');
            location.reload();
          },
          err => {
            this.toast.error('Verifique com o administrador se o seu usuario possui permissão', 'Ocorreu um erro ao tentar excluir o registro');
          }
        )
    } else {
      return;
    }
  }

  sendSaveRequestOferta() {
    var result,
      ofertaValue = this.formOferta.value;

    if (ofertaValue.id) {
      console.log("UPDATE")
      result = this.ofertaService.updateOferta(ofertaValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Oferta editada com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
          }
        );
    } else {
      console.log("ADICIONAR")
      result = this.ofertaService.addOferta(ofertaValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Oferta lançada com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar lançar uma nova oferta');
          }
        );
    }
  }

  excluirOferta(oferta) {
    let resp = confirm('Deseja mesmo excluir esta oferta ?');
    if (resp === true) {
      this.ofertaService.deleteOferta(oferta.id)
        .subscribe(
          suc => {
            this.toast.warning('', 'Registro excluido com sucesso. ');
            location.reload();
            
          },
          err => {
            this.toast.error('Verifique com o administrador se o seu usuario possui permissão', 'Ocorreu um erro ao tentar excluir o registro');
          }
        )
    } else {
      return;
    }
  }

  sendSaveRequestMissao() {
    var result,
      missaoValue = this.formMissao.value;

    if (missaoValue.id) {
      console.log("UPDATE")
      result = this.missaoService.updateMissao(missaoValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Oferta de missões editada com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
          }
        );
    } else {
      console.log("ADICIONAR")
      result = this.missaoService.addMissao(missaoValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Oferta de missões lançada com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar lançar uma nova oferta missionária');
          }
        );
    }
  }

  excluirMissao(missao) {
    let resp = confirm('Deseja mesmo excluir esta oferta de misssões ?');
    if (resp === true) {
      this.missaoService.deleteMissao(missao.id)
      .subscribe(
          suc => {
            this.toast.warning('', 'Registro excluido com sucesso. ');
            location.reload();
          },
          err => {
            this.toast.error('Verifique com o administrador se o seu usuario possui permissão', 'Ocorreu um erro ao tentar excluir o registro');
          }
        )
    } else {
      return;
    }
  }

  
  sendSaveRequestConta() {
    var result,
      contaValue = this.formConta.value;

    if (contaValue.id) {
      console.log("UPDATE")
      result = this.contaService.updateConta(contaValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Conta editada com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
          }
        );
    } else {
      console.log("ADICIONAR")
      result = this.contaService.addConta(contaValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Conta cadastrada com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar cadastrar uma nova conta');
          }
        );
    }
  }

  excluirConta(conta) {
    let resp = confirm('Deseja mesmo excluir esta conta ?');
    if (resp === true) {
      this.contaService.deleteConta(conta.id)
        .subscribe(
          suc => {
            this.toast.warning('', 'Registro excluido com sucesso. ');
            location.reload();
          },
          err => {
            this.toast.error('Verifique com o administrador se o seu usuario possui permissão', 'Ocorreu um erro ao tentar excluir o registro');
            
          }
        )
    } else {
      return;
    }
  }

  initializeObjects(){
    this.pessoaService.getPessoas()
      .subscribe(pessoas => this.pessoas = pessoas);

    this.dizimoService.getDizimos()
      .subscribe(res => {
        this.dataSourceDizimo.data = res as Dizimo[];
      });

    this.ofertaService.getOfertas()
      .subscribe(res => {
        this.dataSourceOferta.data = res as Oferta[];
      });

    this.missaoService.getMissoes()
      .subscribe(res => {
        this.dataSourceMissao.data = res as Missao[];
      });

    this.contaService.getContas()
      .subscribe(res => {
        this.dataSourceConta.data = res as Conta[];
      });
  }
}
