import { NgModule } from "@angular/core";
import { MaterialModule } from "app/angular-material/material-module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MovimentacaoRoutingModule } from "./movimentacao.routing.module";
import { MovimentacoesComponent } from "./movimentacoes.component";
import { ContaFilterPipe } from "./filter/conta.filter";
import { MissaoFilterPipe } from "./filter/missao.filter";
import { OfertaFilterPipe } from "./filter/oferta.filter";
import { DizimoFilterPipe } from "./filter/dizimo.filter";
import { ContaService } from "./service/conta.service";
import { MissaoService } from "./service/missao.service";
import { OfertaService } from "./service/oferta.service";
import { DizimoService } from "./service/dizimo.service";
import { LbdModule } from "app/lbd/lbd.module";
import { SidebarModule } from "app/sidebar/sidebar.module";
import { NavbarModule } from "app/shared/navbar/navbar.module";
import { FooterModule } from "app/shared/footer/footer.module";
import { PessoaService } from "../pessoa/service/pessoa.service";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "app/auth-interceptor";

@NgModule({
    imports: [
        HttpClientModule,
        MaterialModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LbdModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        MovimentacaoRoutingModule

    ],
    declarations: [
        MovimentacoesComponent,
        ContaFilterPipe,
        MissaoFilterPipe,
        OfertaFilterPipe,
        DizimoFilterPipe
    ],
    providers: [
        ContaService,
        MissaoService,
        OfertaService,
        DizimoService,
        PessoaService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ]
})
export class MovimentacaoModule {}