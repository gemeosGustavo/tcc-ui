import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { MovimentacoesComponent } from "./movimentacoes.component";
import { AuthGuard } from "app/guard/AuthGuard";


const movimentacaoRoutes: Routes = [
    { path: '', component: MovimentacoesComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(movimentacaoRoutes)],
    exports: [RouterModule]
})

export class MovimentacaoRoutingModule {
    
}