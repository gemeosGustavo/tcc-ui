import { TestBed } from '@angular/core/testing';

import { DizimoService } from './dizimo.service';

describe('DizimoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DizimoService = TestBed.get(DizimoService);
    expect(service).toBeTruthy();
  });
});
