import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Oferta } from 'app/domain/oferta/oferta';

@Injectable()
export class OfertaService {

  private url: string = 'http://localhost:7765/oferta';

  constructor(private http: HttpClient) { }

  getOfertas(): Observable<Oferta[]>{
    return this.http.get<Oferta[]>(this.url + "/dados");
  }

  getOferta(id): Observable<Oferta>{
    return this.http.get<Oferta>(this.url + "/dados/" + id);
  }

  addOferta(oferta){
    return this.http.post(this.url + "/adicionar", JSON.stringify(oferta));
  }

  updateOferta(oferta){
    return this.http.put(this.url + "/atualizar/" + oferta.id, JSON.stringify(oferta));
  }

  deleteOferta(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}

