import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Conta } from 'app/domain/conta/conta';

@Injectable()
export class ContaService {

  private url: string = 'http://localhost:7765/conta';

  constructor(private http: HttpClient) { }

  getContas(): Observable<Conta[]>{
    return this.http.get<Conta[]>(this.url + "/dados");
  }

  getConta(id): Observable<Conta>{
    return this.http.get<Conta>(this.url + "/dados/" + id);
  }

  addConta(conta){
    return this.http.post(this.url + "/adicionar", JSON.stringify(conta));
  }

  updateConta(conta){
    return this.http.put(this.url + "/atualizar/" + conta.id, JSON.stringify(conta));
  }

  deleteConta(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}
