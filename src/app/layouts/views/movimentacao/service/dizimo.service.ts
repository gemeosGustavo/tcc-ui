import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dizimo } from 'app/domain/dizimo/dizimo';

@Injectable()
export class DizimoService {

  private url: string = 'http://localhost:7765/dizimo';

  constructor(private http: HttpClient) { }

  getDizimos(): Observable<Dizimo[]>{
    return this.http.get<Dizimo[]>(this.url + "/dados");
  }

  getDizimo(id): Observable<Dizimo>{
    return this.http.get<Dizimo>(this.url + "/dados/" + id);
  }

  addDizimo(dizimo){
    return this.http.post(this.url + "/adicionar", JSON.stringify(dizimo));
  }

  updateDizimo(dizimo){
    return this.http.put(this.url + "/atualizar/" + dizimo.id, JSON.stringify(dizimo));
  }

  deleteDizimo(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}
