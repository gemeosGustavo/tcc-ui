import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Missao } from 'app/domain/missao/missao';

@Injectable()
export class MissaoService {

  private url: string = 'http://localhost:7765/missao';

  constructor(private http: HttpClient) { }

  getMissoes(): Observable<Missao[]>{
    return this.http.get<Missao[]>(this.url + "/dados");
  }

  getMissao(id): Observable<Missao>{
    return this.http.get<Missao>(this.url + "/dados/" + id);
  }

  addMissao(missao){
    return this.http.post(this.url + "/adicionar", JSON.stringify(missao));
  }

  updateMissao(missao){
    return this.http.put(this.url + "/atualizar/" + missao.id, JSON.stringify(missao));
  }

  deleteMissao(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}
