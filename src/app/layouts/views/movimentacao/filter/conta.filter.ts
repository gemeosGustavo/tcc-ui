import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'contaFilter'})
export class ContaFilterPipe implements PipeTransform{
    transform(value: any[], term: string): any[]{
        return value.filter((x:any) => x.descricao.toLowerCase().startsWith(term.toLowerCase()))
    }
}