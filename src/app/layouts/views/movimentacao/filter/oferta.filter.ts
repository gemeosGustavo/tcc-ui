import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'ofertaFilter'})
export class OfertaFilterPipe implements PipeTransform{
    transform(value: any[], term: string): any[]{
        return value.filter((x:any) => x.valor_oferta.toLowerCase().startsWith(term.toLowerCase()))
    }
}