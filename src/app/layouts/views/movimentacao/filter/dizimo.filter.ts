import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'dizimoFilter'})
export class DizimoFilterPipe implements PipeTransform{
    transform(value: any[], term: string): any[]{
        return value.filter((x:any) => x.data_dizimo.toLowerCase().startsWith(term.toLowerCase()))
    }
}