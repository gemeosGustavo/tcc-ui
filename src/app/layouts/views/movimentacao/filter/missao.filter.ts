import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'missaoFilter'})
export class MissaoFilterPipe implements PipeTransform{
    transform(value: any[], term: string): any[]{
        return value.filter((x:any) => x.valor_missao.toLowerCase().startsWith(term.toLowerCase()))
    }
}