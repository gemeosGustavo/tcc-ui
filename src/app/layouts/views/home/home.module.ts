import { NgModule } from "@angular/core";
import { MaterialModule } from "app/angular-material/material-module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HomeRoutingModule } from "./home.routing.module";
import { HomeComponent } from "./home.component";
import { LbdModule } from "app/lbd/lbd.module";
import { SidebarModule } from "app/sidebar/sidebar.module";
import { NavbarModule } from "app/shared/navbar/navbar.module";
import { FooterModule } from "app/shared/footer/footer.module";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RelatorioContasService } from "./service/relatorio-contas.service";
import { RelatorioDizimosService } from "./service/relatorio-dizimos.service";
import { RelatorioMissoesService } from "./service/relatorio-missoes.service";
import { RelatorioOfertasService } from "./service/relatorio-ofertas.service";
import { AuthInterceptor } from "app/auth-interceptor";
import { RelatorioSalarioService } from "./service/relatorio-salario.service";


@NgModule({
    imports: [
        HttpClientModule,
        MaterialModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LbdModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        HomeRoutingModule

    ],
    declarations: [
        HomeComponent
    ],
    providers: [
        RelatorioContasService,
        RelatorioDizimosService,
        RelatorioMissoesService,
        RelatorioOfertasService,
        RelatorioSalarioService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
        
    ]
})
export class HomeModule {}