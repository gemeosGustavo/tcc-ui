import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RelatorioContasService } from './service/relatorio-contas.service';
import { RelatorioDizimosService } from './service/relatorio-dizimos.service';
import { RelatorioMissoesService } from './service/relatorio-missoes.service';
import { RelatorioOfertasService } from './service/relatorio-ofertas.service';
import { Relatorio } from 'app/domain/relatorios/relatorio';
import { RelatorioSalarioService } from './service/relatorio-salario.service';
import { Conta } from 'app/domain/conta/conta';
import { Dizimo } from 'app/domain/dizimo/dizimo';
import { Missao } from 'app/domain/missao/missao';
import { Oferta } from 'app/domain/oferta/oferta';
import { MatTableDataSource } from '@angular/material';
import { Salario } from 'app/domain/salario/salario';
import { timingSafeEqual } from 'crypto';
import { timeout } from 'q';
import { enterView } from '@angular/core/src/render3/state';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  saida_caixa: number;
  entrada_caixa: number;

  formRelatorio: FormGroup;
  relatorio: Relatorio = new Relatorio();

  conta: any;
  dizimo: any;
  missao: any;
  oferta: any;
  salario: any;

  contaTotal: Conta = new Conta();
  contas: Conta[];
  displayedColumnsContas = ['valorConta', 'dataPagamento', 'descricao'];
  dataSourceConta: MatTableDataSource<Conta>;

  salarioTotal: Salario = new Salario();
  salarios: Salario[];
  displayedColumnsSalario = ['data_pagamento_salario', 'valor_pagamento', 'descricao_pagamento'];
  dataSourceSalario: MatTableDataSource<Salario>;

  dizimoTotal: Dizimo = new Dizimo();
  dizimos: Dizimo[];
  displayedColumnsDizimos = ['pessoaid', 'valorDizimo', 'dataDizimo'];
  dataSourceDizimo: MatTableDataSource<Dizimo>;

  missaoTotal: Missao = new Missao();
  missoes: Missao[];
  displayedColumnsMissoes = ['valorMissao', 'dataOfertaMissao'];
  dataSourceMissao: MatTableDataSource<Missao>;

  ofertaTotal: Oferta = new Oferta();
  ofertas: Oferta[];
  displayedColumnsOferta = ['valorOferta', 'dataOferta'];
  dataSourceOferta: MatTableDataSource<Oferta>;


  constructor(
    formBuilder: FormBuilder,
    private toast: ToastrService,
    private relatorio_contas: RelatorioContasService,
    private relatorio_dizimos: RelatorioDizimosService,
    private relatorio_missoes: RelatorioMissoesService,
    private relatorio_ofertas: RelatorioOfertasService,
    private relatorio_salarios: RelatorioSalarioService

  ) {
    this.formRelatorio = formBuilder.group({
      data_inicio: ['', Validators.required],
      data_fim: ['', Validators.required]
    });

    this.dataSourceConta = new MatTableDataSource(this.contas);
    this.dataSourceSalario = new MatTableDataSource(this.salarios);
    this.dataSourceDizimo = new MatTableDataSource(this.dizimos);
    this.dataSourceMissao = new MatTableDataSource(this.missoes);
    this.dataSourceOferta = new MatTableDataSource(this.ofertas);
  }

  mostraRelatorio() {
    setTimeout(() => {

      this.somaValores();
      this.relatorioTotal();

      this.graficos();

      $("#cardGraficoGeral").toggle("slow");
      $("#cardGraficoBruto").toggle("slow");
      $("#relatorioContas").toggle("fast");
      $("#relatorioSalarios").toggle("fast");
      $("#relatorioDizimos").toggle("fast");
      $("#relatorioMissoes").toggle("fast");
      $("#relatorioOfertas").toggle("fast");

  }, 600);

  }

  somaValores() {

    var relatorioValue = this.formRelatorio.value;

    this.relatorio_contas.getRelatorioContas(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(object => this.conta = object);

    this.relatorio_dizimos.getRelatorioDizimos(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(object1 => this.dizimo = object1);

    this.relatorio_missoes.getRelatorioMissao(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(object2 => this.missao = object2);

    this.relatorio_ofertas.getRelatorioOfertas(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(object3 => this.oferta = object3);

    this.relatorio_salarios.getRelatorioSalarios(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(object4 => this.salario = object4);

  }

  relatorioTotal() {
    var relatorioValue = this.formRelatorio.value;

    this.relatorio_contas.getRelatorioContasTabela(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(res => {
        this.dataSourceConta.data = res as Conta[];
      });

    this.relatorio_dizimos.getRelatorioDizimosTabela(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(res => {
        this.dataSourceDizimo.data = res as Dizimo[];
      });

    this.relatorio_missoes.getRelatorioMissaoTabela(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(res => {
        this.dataSourceMissao.data = res as Missao[];
      });

    this.relatorio_ofertas.getRelatorioOfertasTabela(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(res => {
        this.dataSourceOferta.data = res as Oferta[];
      });

    this.relatorio_salarios.getRelatorioSalariosTabela(relatorioValue.data_inicio, relatorioValue.data_fim)
      .subscribe(res => {
        this.dataSourceSalario.data = res as Salario[];
      });
  }

  graficos() {
    setTimeout(() => {

      let chart = am4core.create("graficoGeral", am4charts.PieChart);
      let series = chart.series.push(new am4charts.PieSeries());
      series.dataFields.value = "valor";
      series.dataFields.category = "movimentacao";
      chart.data = [{
        "movimentacao": "Contas",
        "valor": this.conta
      }, {
        "movimentacao": "Salarios",
        "valor": this.salario
      }, {
        "movimentacao": "Dizimos",
        "valor": this.dizimo
      }, {
        "movimentacao": "Missoes",
        "valor": this.missao
      }, {
        "movimentacao": "Ofertas",
        "valor": this.oferta
      }];
      chart.legend = new am4charts.Legend();


      this.entrada_caixa = parseFloat(this.dizimo) + parseFloat(this.oferta) + parseFloat(this.missao);

      this.saida_caixa = parseFloat(this.conta) + parseFloat(this.salario);

        console.log("dizimo"+ this.dizimo);
        console.log("oferta"+ this.oferta);
        console.log("missao"+ this.missao);
        console.log(this.entrada_caixa);
        console.log("salario"+ this.salario);
        console.log("conta"+ this.conta);
        console.log(this.saida_caixa);
      

      let chart1 = am4core.create("graficoBruto", am4charts.PieChart);
      let series2 = chart1.series.push(new am4charts.PieSeries());
      series2.dataFields.value = "total";
      series2.dataFields.category = "movimentacao";
      chart1.data = [{
        "movimentacao": "Entrada",
        "total": this.entrada_caixa
      }, {
        "movimentacao": "Saída",
        "total": this.saida_caixa
      }];
      chart1.legend = new am4charts.Legend();

    }, 200);

  }

  ngOnInit() {
    $("#relatorioContas").hide();
    $("#relatorioSalarios").hide();
    $("#relatorioDizimos").hide();
    $("#relatorioMissoes").hide();
    $("#relatorioOfertas").hide();
    $("#cardGraficoGeral").hide();
    $("#cardGraficoBruto").hide();
  }

}
