import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ResultadoMissao } from "app/domain/relatorios/resultado_missao";
import { Missao } from "app/domain/missao/missao";

@Injectable()
export class RelatorioMissoesService {

  private url: string = 'http://localhost:7765/missao/relatorio_missao';

  constructor(private http: HttpClient) { }

  getRelatorioMissao(data_inicio, data_fim): Observable<ResultadoMissao>{
    return this.http.get<ResultadoMissao>(this.url + "/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

  getRelatorioMissaoTabela(data_inicio, data_fim): Observable<Missao[]>{
    return this.http.get<Missao[]>(this.url + "/total/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

}