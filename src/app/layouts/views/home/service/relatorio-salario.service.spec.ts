import { TestBed } from '@angular/core/testing';

import { RelatorioSalarioService } from './relatorio-salario.service';

describe('SalarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RelatorioSalarioService = TestBed.get(RelatorioSalarioService);
    expect(service).toBeTruthy();
  });
});
