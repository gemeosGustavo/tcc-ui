import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResultadoDizimo } from 'app/domain/relatorios/resultado_dizimo';
import { Dizimo } from 'app/domain/dizimo/dizimo';


@Injectable()
export class RelatorioDizimosService {

  private url: string = 'http://localhost:7765/dizimo/relatorio_dizimo';

  constructor(private http: HttpClient) { }

  getRelatorioDizimos(data_inicio, data_fim): Observable<ResultadoDizimo>{
    return this.http.get<ResultadoDizimo>(this.url + "/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

  getRelatorioDizimosTabela(data_inicio, data_fim): Observable<Dizimo[]>{
    return this.http.get<Dizimo[]>(this.url + "/total/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }
}