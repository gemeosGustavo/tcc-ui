import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ResultadoOferta } from "app/domain/relatorios/resultado_oferta";
import { Oferta } from "app/domain/oferta/oferta";

@Injectable()
export class RelatorioOfertasService {

  private url: string = 'http://localhost:7765/oferta/relatorio_oferta';

  constructor(private http: HttpClient) { }

  getRelatorioOfertas(data_inicio, data_fim): Observable<ResultadoOferta>{
    return this.http.get<ResultadoOferta>(this.url + "/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

  getRelatorioOfertasTabela(data_inicio, data_fim): Observable<Oferta[]>{
    return this.http.get<Oferta[]>(this.url + "/total/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

}