import { TestBed } from '@angular/core/testing';

import { RelatorioMissoesService } from './relatorio-missoes.service';

describe('RelatorioMissoesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RelatorioMissoesService = TestBed.get(RelatorioMissoesService);
    expect(service).toBeTruthy();
  });
});
