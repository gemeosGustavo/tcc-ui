import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ResultadoSalario } from "app/domain/relatorios/resultado_salario";
import { Salario } from "app/domain/salario/salario";

@Injectable()
export class RelatorioSalarioService {

  private url: string = 'http://localhost:7765/salario/relatorio_salario';

  constructor(private http: HttpClient) { }

  getRelatorioSalarios(data_inicio, data_fim): Observable<ResultadoSalario>{
    return this.http.get<ResultadoSalario>(this.url + "/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

  getRelatorioSalariosTabela(data_inicio, data_fim): Observable<Salario[]>{
    return this.http.get<Salario[]>(this.url + "/total/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

}