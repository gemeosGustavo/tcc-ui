import { TestBed } from '@angular/core/testing';

import { RelatorioOfertasService } from './relatorio-ofertas.service';

describe('RelatorioOfertasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RelatorioOfertasService = TestBed.get(RelatorioOfertasService);
    expect(service).toBeTruthy();
  });
});
