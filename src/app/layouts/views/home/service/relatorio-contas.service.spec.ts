import { TestBed } from '@angular/core/testing';

import { RelatorioContasService } from './relatorio-contas.service';

describe('RelatorioContasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RelatorioContasService = TestBed.get(RelatorioContasService);
    expect(service).toBeTruthy();
  });
});
