import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResultadoConta } from 'app/domain/relatorios/resultado_conta';
import { Conta } from 'app/domain/conta/conta';


@Injectable()
export class RelatorioContasService {

  private url: string = 'http://localhost:7765/conta/relatorio_conta';

  constructor(private http: HttpClient) { }

  getRelatorioContas(data_inicio, data_fim): Observable<ResultadoConta>{
    return this.http.get<ResultadoConta>(this.url + "/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

  getRelatorioContasTabela(data_inicio, data_fim): Observable<Conta[]>{
    return this.http.get<Conta[]>(this.url + "/total/data_inicio=" + data_inicio + "/data_fim=" + data_fim);
  }

}