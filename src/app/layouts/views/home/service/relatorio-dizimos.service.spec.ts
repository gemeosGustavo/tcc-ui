import { TestBed } from '@angular/core/testing';

import { RelatorioDizimosService } from './relatorio-dizimos.service';

describe('RelatorioDizimosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RelatorioDizimosService = TestBed.get(RelatorioDizimosService);
    expect(service).toBeTruthy();
  });
});
