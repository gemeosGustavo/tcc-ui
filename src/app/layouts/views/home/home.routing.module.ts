import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { HomeComponent } from "./home.component";
import { AuthGuard } from "app/guard/AuthGuard";


const homeRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})

export class HomeRoutingModule {
    
}