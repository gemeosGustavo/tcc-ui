import { NgModule } from "@angular/core";
import { MaterialModule } from "app/angular-material/material-module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { UserRoutingModule } from "./user.routing.module";
import { UserComponent } from "./user.component";
import { LbdChartComponent } from "app/lbd/lbd-chart/lbd-chart.component";
import { LbdModule } from "app/lbd/lbd.module";
import { SidebarModule } from "app/sidebar/sidebar.module";
import { NavbarModule } from "app/shared/navbar/navbar.module";
import { FooterModule } from "app/shared/footer/footer.module";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { UserService } from "./service/user.service";
import { AuthInterceptor } from "app/auth-interceptor";
import { UsuarioFilterPipe } from "./filter/usuario.filter";

@NgModule({
    imports: [
        MaterialModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LbdModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        HttpClientModule,
        UserRoutingModule

    ],
    declarations: [
        UserComponent,
        UsuarioFilterPipe
    ],
    providers: [
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ]
})
export class UserModule {}