import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { UserComponent } from "./user.component";
import { AuthGuard } from "app/guard/AuthGuard";


const userRoutes: Routes = [
    { path: '', component: UserComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(userRoutes)],
    exports: [RouterModule]
})

export class UserRoutingModule {
    
}