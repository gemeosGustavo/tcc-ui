import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Usuario } from 'app/domain/usuario/usuario';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

  private url: string = 'http://localhost:7765/usuario';

  constructor(private http: HttpClient) { }

  getUsuarios(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(this.url + "/dados");
  }

  getUsuario(id): Observable<Usuario>{
    return this.http.get<Usuario>(this.url + "/dados/" + id);
  }

  addUsuario(usuario){
    return this.http.post(this.url + "/singup", JSON.stringify(usuario));
  }

  updateUsuario(usuario){
    return this.http.put(this.url + "/atualizar/" + usuario.id, JSON.stringify(usuario));
  }

}


  

 
