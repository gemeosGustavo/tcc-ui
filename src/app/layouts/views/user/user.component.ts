import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuario } from 'app/domain/usuario/usuario';
import { ToastrService } from 'ngx-toastr';
import { UserService } from './service/user.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Permissao } from 'app/domain/permissao/permissao';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  formUsuario: FormGroup;
  roles: Permissao[];
  usuario: Usuario = new Usuario();
  usuarios: Usuario[];

  displayedColumns = ['cod', 'nome', 'username', 'editar'];
  dataSource: MatTableDataSource<Usuario>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    formBuilder: FormBuilder,
    private toast: ToastrService,
    private usuarioService: UserService,
  ) { 
    this.formUsuario = formBuilder.group({
      id: [''],
      nome: ['', Validators.required],
      sobrenome: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      permissao: ['', Validators.required]
    });
    this.dataSource = new MatTableDataSource(this.usuarios);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    $("#usuario").hide();
    this.initializeObjects();
  }

  cadastrarUsuarios(){
    $("#usuario").toggle("slow");
  }

  editar(usuario) {
    $("#usuario").toggle("slow");
    this.popularDadosForm(usuario);
  }

  popularDadosForm(dados) {
    this.formUsuario.patchValue({
      id: dados.id,
      nome: dados.nome,
      sobrenome: dados.sobrenome,
      username: dados.username,
      password: dados.password,
      permissao: dados.permissao,
    });
  }

  sendSaveRequest() {
    var result,
      usuarioValue = this.formUsuario.value;

    if (usuarioValue.id) {
      console.log("UPDATE")
      result = this.usuarioService.updateUsuario(usuarioValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Usuário editado com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
          }
        );
    } else {
      console.log("ADICIONAR")  
      result = this.usuarioService.addUsuario(usuarioValue)
        .subscribe(
          suc => {
            this.toast.success('', 'Usuário cadastrado com sucesso');
            location.reload();
          },
          err => {
            this.toast.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar cadastrar um novo usuário');
          }
        );
    }
  }

  initializeObjects() {
    this.usuarioService.getUsuarios()
    .subscribe(res => {
      this.dataSource.data = res as Usuario[];
    });
  }

}
