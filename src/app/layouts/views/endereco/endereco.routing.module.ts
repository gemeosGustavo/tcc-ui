import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { EnderecoComponent } from './endereco.component';
import { AuthGuard } from "app/guard/AuthGuard";


const enderecoRoutes: Routes = [
    { path: '', component: EnderecoComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [RouterModule.forChild(enderecoRoutes)],
    exports: [RouterModule]
})

export class EnderecoRoutingModule {
    
}