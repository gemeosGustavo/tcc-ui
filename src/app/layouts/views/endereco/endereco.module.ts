import { NgModule } from "@angular/core";
import { EnderecoService } from "./service/endereco.service";
import { EnderecoComponent } from "./endereco.component";
import { MaterialModule } from "app/angular-material/material-module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { EnderecoRoutingModule } from "./endereco.routing.module";
import { EnderecoFilterPipe } from "./filter/endereco.filter";
import { LbdModule } from "app/lbd/lbd.module";
import { SidebarModule } from "app/sidebar/sidebar.module";
import { NavbarModule } from "app/shared/navbar/navbar.module";
import { FooterModule } from "app/shared/footer/footer.module";
import { ToastService } from "app/toastr-service";
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "app/auth-interceptor";

@NgModule({
    imports: [
        HttpClientModule,
        MaterialModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LbdModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        EnderecoRoutingModule,

    ],
    declarations: [
        EnderecoComponent,
        EnderecoFilterPipe,
    ],
    providers: [
        EnderecoService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
      
    ]
})
export class EnderecoModule {}