import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Endereco } from 'app/domain/endereco/endereco';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { EnderecoService } from './service/endereco.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  styleUrls: ['./endereco.component.scss']
})
export class EnderecoComponent implements OnInit {

  formEndereco: FormGroup;
  endereco: Endereco = new Endereco();
  enderecos: Endereco[];
  query:string = ''
  
  displayedColumns = ['cod', 'uf', 'cidade', 'bairro', 'logradouro', 'editar', 'excluir'];
  dataSource: MatTableDataSource<Endereco>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor(
    formBuilder: FormBuilder,
    private toastr: ToastrService,
    private enderecoService: EnderecoService
  ) { 
    this.formEndereco = formBuilder.group({
      id: [''],
      logradouro: ['', Validators.required],
      bairro: ['', Validators.required],
      cidade: ['', Validators.required],
      uf:['', Validators.required]
    });
  
  this.dataSource = new MatTableDataSource(this.enderecos);
  }
  
  ngOnInit() {

    this.initializeObjects();
  
    $("#endereco").hide();
    $("#listaEnderecos").hide();
  
  }
  
  mostrarEnderecos(){
    $("#listaEnderecos").toggle("slow");
  }
  
  mostrarCadastroEndereco(){
    $("#endereco").toggle("slow");
  }

  popularDadosForm(dados) {
    this.formEndereco.patchValue({
      id: dados.id,
      logradouro: dados.logradouro,
      bairro: dados.bairro,
      cidade: dados.cidade,
      uf: dados.uf
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  
  sendSaveRequest(){
    var result,
    enderecoValue = this.formEndereco.value;
  
    if(enderecoValue.id){
    console.log("UPDATE")
    result = this.enderecoService.updateEndereco(enderecoValue)
    .subscribe(
      suc => {
        this.toastr.success('', 'Endereço editado com sucesso');
        location.reload();
      },
      err => {
        this.toastr.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar salvar');
      }
    );

  } else {
      console.log("ADICIONAR")
      result = this.enderecoService.addEndereco(enderecoValue)
      .subscribe(
        suc => {
          this.toastr.success('', 'Endereço cadastrado com sucesso');
          location.reload();
        },
        err => {
          this.toastr.error('Verifique se todos os campos foram preenchidos corretamente, ou se você possui permissão de acesso.', 'Ocorreu um erro ao tentar cadastrar um novo endereço');
        }
      );
    }
  }

  editar(endereco){
    $("#endereco").toggle("slow");
    this.popularDadosForm(endereco);
  }
  
  excluir(endereco){
    let resp = confirm('Deseja mesmo excluir este endereço ?');
    if (resp === true) {
      this.enderecoService.deleteEndereco(endereco.id)
        .subscribe(
          suc => {
            this.toastr.warning('', 'Registro excluido com sucesso. ');
            this.initializeObjects();
          },
          err => {
            this.toastr.error('Verifique com o administrador se o seu usuario possui permissão', 'Ocorreu um erro ao tentar excluir o registro');
          }
        )

    } else {
      return;
    }
  }

  initializeObjects(){
    this.enderecoService.getEnderecos()
    .subscribe(res => {
      this.dataSource.data = res as Endereco[];
    });
  }

}
  