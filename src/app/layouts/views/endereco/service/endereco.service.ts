import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Endereco } from 'app/domain/endereco/endereco';

@Injectable()
export class EnderecoService {

  private url: string = 'http://localhost:7765/endereco';

  constructor(private http: HttpClient) { }

  getEnderecos(): Observable<Endereco[]>{
    return this.http.get<Endereco[]>(this.url + "/dados");
  }

  getEndereco(id): Observable<Endereco>{
    return this.http.get<Endereco>(this.url + "/dados/" + id);
  }

  addEndereco(endereco){
    return this.http.post(this.url + "/adicionar", JSON.stringify(endereco));
  }

  updateEndereco(endereco){
    return this.http.put(this.url + "/atualizar/" + endereco.id, JSON.stringify(endereco));
  }

  deleteEndereco(id) {
    return this.http.delete(this.url + "/deletar/" + id);
  }
}
