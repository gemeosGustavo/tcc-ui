import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LbdModule } from '../../lbd/lbd.module';

import { AdminLayoutComponent } from './admin-layout.component';
import { NavbarModule } from 'app/shared/navbar/navbar.module';
import { FooterModule } from 'app/shared/footer/footer.module';
import { SidebarModule } from 'app/sidebar/sidebar.module';
import { AdminLayoutRoutingModule } from './admin-layout.routing.module';



@NgModule({
  imports: [
    NavbarModule,
    FooterModule,
    SidebarModule,
    CommonModule,
    FormsModule,
    AdminLayoutRoutingModule,
    ReactiveFormsModule,
    LbdModule
  ],
  declarations: [
    AdminLayoutComponent

  ],
  providers: [
  ]
})

export class AdminLayoutModule {}
