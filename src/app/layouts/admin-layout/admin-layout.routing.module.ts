import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { AdminLayoutComponent } from './admin-layout.component';
import { AuthGuard } from 'app/guard/AuthGuard';

export const AdminLayoutRoutes: Routes = [
    {   path: '', component: AdminLayoutComponent, canActivate: [AuthGuard], children: [
        { path: 'relatorio',        loadChildren: '../views/home/home.module#HomeModule'},
        { path: 'usuario',          loadChildren: '../views/user/user.module#UserModule'},
        { path: 'pessoas',          loadChildren: '../views/pessoa/pessoa.module#PessoaModule',},
        { path: 'endereco',         loadChildren: '../views/endereco/endereco.module#EnderecoModule',},
        { path: 'funcionario',      loadChildren: '../views/funcionario/funcionario.module#FuncionarioModule',},
        { path: 'movimentacoes',    loadChildren: '../views/movimentacao/movimentacao.module#MovimentacaoModule',},
    ]}

];

@NgModule({
    imports: [RouterModule.forChild(AdminLayoutRoutes)],
    exports: [RouterModule]
})

export class AdminLayoutRoutingModule {

}