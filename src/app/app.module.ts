import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { routing } from './app.routing';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule } from './shared/navbar/navbar.module';
import { AdminLayoutRoutingModule } from './layouts/admin-layout/admin-layout.routing.module';
import { LoginComponent } from './login/login.component';
import { loginRoutingModule } from './login/login.routing.module';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { CommonModule } from '@angular/common';
import { LbdModule } from './lbd/lbd.module';
import { ToastrModule } from 'ngx-toastr';
import { AuthService } from './login/service/auth.service';
import { AuthInterceptor } from './auth-interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenStorageService } from './login/service/token-storage.service';
import { AuthGuard } from './guard/AuthGuard';
import {NgxMaskModule} from 'ngx-mask'

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AdminLayoutRoutingModule,
    loginRoutingModule,
    routing,
    NavbarModule,
    FooterModule,
    SidebarModule,
    CommonModule,
    LbdModule,
    ToastrModule.forRoot(),
    NgxMaskModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    AdminLayoutComponent
  ],
  providers: [
    AuthService,   
    AuthGuard,
    TokenStorageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }


  ],
  bootstrap: [
    AppComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AppModule { }
