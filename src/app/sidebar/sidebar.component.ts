import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/relatorio', title: 'Relatórios',  icon: 'pe-7s-graph', class: '' },
    { path: '/usuario', title: 'Usuário',  icon:'pe-7s-user', class: '' },
    { path: '/endereco', title: 'Endereço',  icon:'pe-7s-map-marker', class: '' },
    { path: '/pessoas', title: 'Pessoas',  icon:'pe-7s-users', class: '' },
    { path: '/funcionario', title: 'Funcionários',  icon:'pe-7s-id', class: '' },
    { path: '/movimentacoes', title: 'Movimentações',  icon:'pe-7s-cash', class: '' },
    { path: '/sobre', title: 'Sobre nós',  icon:'pe-7s-rocket', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
