import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guard/AuthGuard';

const routes: Routes =[
  { path: 'login', component: LoginComponent },
  { path: 'layout', loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
 